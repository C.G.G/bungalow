(function ($) {$(document).ready(function() {
	
  var $window = $(window),
      $logo = $('#logo'),
      $contentWrapper = $('#content-wrapper'),
      $content = $('.content'),
      $mask = $('.mask'),
      $lightbox = $('.lightbox'),
      $lightboxMask = $('.lightbox-mask'),
      wW = $window.width(),
      wH = $window.height();
  
	$.backstretch("images/bg5.jpg");
  
  // thumbnail click opens lightbox
  $('.thumbs img').on('click', function() {
    $images = $('.lightbox .images'),
    // update lightbox title
    $lightbox.find('h3').html($(this).parent().parent().find('h2').html())
    // loop through thumbs and put into images container
    var $thumbs = $(this).parent();
    var total = $thumbs.find('img').length;
    var loaded = 0;
    var index = $(this).index();
    $thumbs.find('img').each(function() {
      $images.append('<div></div>');
    });
    $thumbs.find('img').each(function() {
      var $img = $(this);
  		$('<img />')
  		.attr('src', $img.attr('src').replace('thumbs', 'large'))
  		.load(function() {
  		  $images.find('div').eq($img.index()).append($(this));
  		  loaded++;
  		  // do cycle
  		  if (loaded == total) {
          $images.cycle({
            fx: 'scrollHorz',
            next: '.next',
            prev: '.prev',
            slideResize: 0,
            timeout: 0,
            speed: 400,
            startingSlide: index,
          });
    		}
      });
    });
    // just in case
    $window.trigger('resize');
    // show lightboxes
    $lightboxMask.show();
    if ($window.scrollTop() > 200) {
      if ($window.height() < 700) {
        $lightbox.css('margin-top', $window.scrollTop() - 130);
      }
      else {
        $lightbox.css('margin-top', $window.scrollTop());
      }
    }
    else {
      $lightbox.css('margin-top', 82);
    }
    $lightbox.show();
  });
  
  // close lightbox
  $lightboxMask.on('click', function() {
    $lightbox.hide();
    $lightboxMask.hide();
    $images.cycle('stop').cycle('destroy');
    $images.remove();
    $('.slides').prepend('<div class="images"></div>');
  });
  
  // close x in lightbox
  $lightbox.find('.close').on('click', function() {
    $lightboxMask.trigger('click');
  });
  
  // window resize
  $window.resize(function() {
    wW = $window.width();
    wH = $window.height();
    var logoOffset = wW < 720 ? 0 : (wW - 720) / 2;
    var section = window.location.hash.replace('#', '');
    var logoOffset = section == '' ? logoOffset : $('#menu').offset().left - 400;
    $logo.css('left', logoOffset);
    var contentOffset = wW < 926 ? 0 : (wW - 926) / 2;
    $contentWrapper.css('left', contentOffset);
    $lightboxMask.css({
      width: wW,
      minHeight: wH,
      height: $(document).height()
    });
    $lightbox.css('left', wW < 906 ? 0 : ((wW - 906) / 2) + 2);
  });
  
  // window hashchange
  $window.hashchange(function() {
    $('#menu a').removeClass('current');
    var section = window.location.hash.replace('#', '');
    // home
    if (section == '') {
      wW = $window.width();
      wH = $window.height();
      $contentWrapper.hide();
      $logo.css({
        height: 381,
        width: 720,
        top: 170,
        left: wW < 720 ? 0 : (wW - 720) / 2
      });
      $('#date, .social, .badge').show();
      $('#logo').attr('href', '#about');
    }
    else { // about, photos, reservations
      $('#menu a[href*="' + window.location.hash + '"]').addClass('current');
      var contentOffset = wW < 926 ? 0 : (wW - 926) / 2;
      if ($('#date').is(':visible') == true) {
        $logo.css({
          height: 220, 
          width: 416, 
          top: -33, 
          left: ($('#menu').offset().left - 400)
        });
        $contentWrapper.show();
        $('#date, .social, .badge').hide();
      }
      loadContent();
      $('#logo').attr('href', '#');
    }
  })
  
  // show correct page
  function loadContent() {
    var section = '.' + window.location.hash.replace('#', '');
    var sectionHeight = $(section).height();
    $content.css({
      top: 0 - ($(section).position().top)
    });
    $mask.css({
      height: sectionHeight
    });
  }
    
  $window.trigger('resize');

  try {
   Typekit.load({
     active: function() {
       $window.trigger('hashchange');
     }
   })
  } catch(e) {}

});})(jQuery);;
